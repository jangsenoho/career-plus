/*
 * @(#) rui_config.js
 * build version : 0.9.2 $Revision: 14723 $
 *
 * Copyright �� LG CNS, Inc. All rights reserved.
 *
 * Do Not Erase This Comment!!! (�� 二쇱꽍臾몄쓣 吏��곗� 留먭쾬)
 *
 * DevOn RUI Framework瑜� �ㅼ젣 �꾨줈�앺듃�� �ъ슜�섎뒗 寃쎌슦 DevOn RUI 媛쒕컻�대떦�먯뿉寃�
 * �꾨줈�앺듃 �뺤떇紐낆묶, �대떦�� �곕씫泥�(Email)�깆쓣 mail濡� �뚮젮�� �쒕떎.
 *
 * �뚯뒪瑜� 蹂�寃쏀븯�� �ъ슜�섎뒗 寃쎌슦 DevOn RUI 媛쒕컻�대떦�먯뿉寃�
 * 蹂�寃쎈맂 �뚯뒪 �꾩껜�� 蹂�寃쎈맂 �ы빆�� �뚮젮�� �쒕떎.
 * ���묒옄�� �쒓났�� �뚯뒪媛� �좎슜�섎떎怨� �먮떒�섎뒗 寃쎌슦 �대떦 �ы빆�� 諛섏쁺�� �� �덈떎.
 * 以묒슂�� Idea瑜� �쒓났�섏��ㅺ퀬 �먮떒�섎뒗 寃쎌슦 �묒쓽�섏뿉 ���� List�� 諛섏쁺�� �� �덈떎.
 *
 * (二쇱쓽!) �먯��먯쓽 �덈씫�놁씠 �щ같�� �� �� �놁쑝硫�
 * LG CNS �몃�濡쒖쓽 �좎텧�� �섏뿬�쒕뒗 �� �쒕떎.
 */
(function() {
  Rui.namespace('Rui.config');
  Rui.namespace('Rui.message.locale');
  /*
   * Web Root媛� '/'媛� �꾨땶寃쎌슦, '/'遺��� Web Root源뚯��� 寃쎈줈瑜� 湲곗닠�쒕떎.  ex) /WebContent
   */
  var contextPath = '/rui',
      ruiRootPath = '/rui2';
  
  contextPath = window._BOOTSTRAP_CONTEXTROOT || contextPath;
  ruiRootPath = window._BOOTSTRAP_RUIROOT || ruiRootPath;
  /*
   * 媛� Module蹂� 湲곕낯 config媛� �ㅼ젙.
   */
  Rui.config.ConfigData = {
      core:
          {
              applicationName: 'DevOn RUI',
              version: 2.7,
              configFileVersion: 1.6,
              /*
               * 湲곕낯 �몄뼱 date, message�깆뿉 �곹뼢�� 以���.
               * */
              defaultLocale: 'ko_KR',
              contextPath: contextPath,
              ruiRootPath: ruiRootPath,
              message: {
                  /*
                   * 湲곕낯 message 媛앹껜
                   */
                  defaultMessage: Rui.message.locale.ko_KR,
                  /*
                   * �ㅺ뎅�� 吏��� message �뚯씪 �꾩튂
                   */
                  localePath: '/resources/locale'
              },
              jsunit: {
                  jsPath: contextPath + '/jsunit/app/jsUnitCore.js'
              },
              css: {
                  charset: 'utf-8'
              },
              logger: {
                  /*
                   * logger 異쒕젰 �щ�
                   */
                  show: false,
                  /*
                   * logger expand �щ�
                   */
                  expand: false,
                  /*
                   * logger 湲곕낯 source 異쒕젰  �щ�
                   */
                  defaultSource: true,
                  /*
                   * logger 湲곕낯 source 紐�
                   */
                  defaultSourceName: 'Component'
              },
              font: {
              },
              debug: {
                  /*
                   * �먮윭 諛쒖깮�� debugging �щ�
                   */
                  exceptionDebugger : false,
                  notice: false,   // 媛쒕컻�먯슜 媛��대뱶 �ъ슜�щ�
                  servers: [ 'localhost', '127.0.0.1']
              },
              useAccessibility: false,
              rightToLeft: false,
              useFixedId: false,  // 而댄룷�뚰듃 el DOM ID 怨좎젙�щ�
              dateLocale: {
                  //ko: {
                  //    x: '%Y��%B��%d��'
                  //}
              }
          }
      ,
      base:
          {
              dataSetManager: {
                  defaultProperties: {
                      params: {abc:123},
                      diableCaching: true,
                      timeout: 30,
                      blankUrl: 'about:blank',
                      defaultSuccessHandler: false,
                      defaultFailureHandler: true,
                      defaultLoadExceptionHandler: true,
                      isCheckedUpdate: true
                  },

                  failureHandler: function(e) {
                      if(typeof loadPanel != 'undefined') loadPanel.hide();
                      if (Rui.getConfig().getFirst('$.core.debug.exceptionDebugger')) {
                          alert('IE�� FireFox�� 寃쎌슦 �붾쾭源� �곹깭濡� �ㅼ젙�⑸땲��.');
                      debugger;
                      }
                      if(e.conn && e.conn.status == -1) // timeout
                          Rui.alert(Rui.getMessageManager().get('$.base.msg112'));
                      else {
                          var dt = new Date();
                          var strDate = dt.getFullYear() + '-' + dt.getMonth() + '-' + dt.getDate();
                          strDate +=  ' ' + dt.getHours() + ':' + dt.getMinutes() + ':' + dt.getSeconds();

                          if(typeof e.conn === 'undefined')
                              strStatus = '';
                          else if(e.conn)
                              strStatus = e.conn.status;
                          else
                              strStatus = e.status;

                          Rui.alert('[RUI-10000]DateTime:[' + strDate + ']:Status:[' + strStatus + ']<br>'
                                  + Rui.getMessageManager().get('$.base.msg101') + ' : ' + e.responseText
                                  + '<br>' + Rui.getMessageManager().get('$.base.msg142'));
                      }
                  },

                  loadExceptionHandler: function(e) {
                      if(typeof loadPanel != 'undefined') loadPanel.hide();
                      if(Rui.getConfig().getFirst('$.core.debug.exceptionDebugger')) {
                          alert('IE�� FireFox�� 寃쎌슦 �붾쾭源� �곹깭濡� �ㅼ젙�⑸땲��.');
                          debugger;
                      }
                      var exception = Rui.getException(e.throwObject);
                      if(e.conn && e.conn.status == -1)  // timeout
                          Rui.alert(Rui.getMessageManager().get('$.base.msg112'));
                      else {
                          var dt = new Date();
                          var strDate = dt.getFullYear() + '-' + dt.getMonth() + '-' + dt.getDate();
                          strDate +=  ' ' + dt.getHours() + ':' + dt.getMinutes() + ':' + dt.getSeconds();

                          if(typeof e.conn === 'undefined')
                              strStatus = '';
                          else
                              strStatus = e.conn.status;

                          Rui.alert('[RUI-10000]DateTime:[' + strDate + ']:Status:[' + strStatus + ']<br>'
                                  + Rui.getMessageManager().get('$.base.msg104') + ' : ' + exception.getMessage()
                                  + '<br>' + Rui.getMessageManager().get('$.base.msg142'));
                      }
                      return false;
                  }
              },
              button: {
                  disableDbClick: true,
                  disableDbClickInterval: 500
              },
              layout: {
                  defaultProperties: {
                  }
              },
              dataSet: {
                  defaultProperties: {
                      params: {abc:123},
                    method: 'GET',
                      focusFirstRow: 0,
                      defaultFailureHandler: true,
                      isClearUnFilterChangeData: false,
                      serializeMetaData: false,
                      readFieldFormater: { date: function(value){
                          if(typeof value == 'number'){
                            return new Date(value);
                          }else
                            return Rui.util.LFormat.stringToTimestamp(value);
                      }},
                      writeFieldFormater: { date: Rui.util.LRenderer.dateRenderer('%Y%m%d%H%M%S') }
                  },
                  loadExceptionHandler: function(e) {
                      if(typeof loadPanel != 'undefined') loadPanel.hide();
                      if(Rui.getConfig().getFirst('$.core.debug.exceptionDebugger')) {
                          alert('IE�� FireFox�� 寃쎌슦 �붾쾭源� �곹깭濡� �ㅼ젙�⑸땲��.');
                          debugger;
                      }
                      var exception = Rui.getException(e.throwObject);
                      var message = e.throwObject ? e.throwObject.message : e.message;
                      Rui.log(message, 'error', this.otype);
                      if(e.conn && e.conn.status == -1)  // timeout
                          Rui.alert(Rui.getMessageManager().get('$.base.msg112'));
                      else{
                          var dt = new Date();
                          var strDate = dt.getFullYear() + '-' + dt.getMonth() + '-' + dt.getDate();
                          strDate +=  ' ' + dt.getHours() + ':' + dt.getMinutes() + ':' + dt.getSeconds();

                          if(typeof e.conn === 'undefined')
                              strStatus = '';
                          else
                              strStatus = e.conn.status;

                          Rui.alert('[RUI-10000]DateTime:[' + strDate + ']:Status:[' + strStatus + ']<br>'
                                  + Rui.getMessageManager().get('$.base.msg104') + ' : ' + exception.getMessage()
                                  + '<br>' + Rui.getMessageManager().get('$.base.msg142'));
                      }
                      return true;
                  }
              },
              guide: {
                  show: true,
                  limitGuideCount: 3,
                  defaultProperties: {
                      showPageGuide: true
                  }
              }
          }
      ,
      ext:
          {
              browser: {
                  recommend: true,
                  recommendCount: 1,
                  link: 'http://browsehappy.com'
              },
              dialog: {
                  defaultProperties: {
                      constraintoviewport: true,
                      fixedcenter: true,
                      modal: true,
                      hideaftersubmit: true,
                      postmethod: 'none'
                  }
              },
              container: {
              },
              textBox: {
                  defaultProperties: {
                      //dataSetClassName: 'Rui.data.LJsonDataSet',
                      filterMode: 'remote',
                      emptyValue: null
                  },
                  dataSet: {
                  }
              },
              combo: {
                  defaultProperties: {
                      filterMode: 'local',
                      //width: 100,
                      //listWidth: 200,
                      //emptyValue: '',
                      useEmptyText: true
                  },
                  dataSet: {
                      //valueField: 'value',
                      //displayField: 'text'
                  }
              },
              multicombo: {
                  defaultProperties: {
                    //�ㅺ뎅�� 泥섎━瑜� �먰븷 寃쎌슦 rui_config.js 媛��� �꾨옒 LMultiCombo �ㅺ뎅�댁쿂由� 諛⑸쾿�� 李몄“
                      placeholder: 'choose a state'
                  }
              },
              numberBox: {
                  defaultProperties: {
                  }
              },
              textArea: {
                  defaultProperties:{
                  }
              },
              checkBox: {
                  defaultProperties: {
                  }
              },
              radio: {
                  defaultProperties: {
                  }
              },
              dateBox: {
                  defaultProperties: {
                      //valueFormat: '%Y-%m-%d',
                    //iconMarginLeft: 1, //input怨� calendar icon �ъ씠 媛꾧꺽 px
                      localeMask: true,
                      calendarConfig: {close: true}
                  }
              },
              timeBox: {
                  defaultProperties: {
                      //iconMarginLeft: 1 //input怨� spin buttons �ъ씠 媛꾧꺽 px
                  }
              },
              monthBox: {
                  defaultProperties: {
                  }
              },
              datetimeBox: {
                  defaultProperties: {
                    //valueFormat: '%Y-%m-%d %H:%M:%S',
                      //iconMarginLeft : 1 //input怨� calendar icon �ъ씠 媛꾧꺽 px
                  }
              },
              fromtodateBox: {
                  defaultProperties: {
                    //valueFormat: '%Y-%m-%d',
                    //separator: '~',
                      //iconMarginLeft : 1 //input怨� calendar icon �ъ씠 媛꾧꺽 px
                  }
              },
              popupTextBox : {
                  defaultProperties:{
                      useHiddenValue: true
                  }
              },
              slideMenu: {
                  defaultProperties: {
                      onlyOneTopOpen: true,
                      defaultOpenTopIndex: 0,
                      fields: {
                          rootValue: null,
                          id: 'menuId',
                          parentId: 'parentMenuId',
                          label: 'name',
                          order: 'seq'
                      }
                  }
              },
              messageBox: {
                  type: 'Rui.ui.MessageBox'
              },
              tabView: {
                  defaultProperties: {
                  }
              },
              treeView: {
                  defaultProperties: {
                      fields: {
                          rootValue: null,
                          id: 'nodeId',
                          parentId: 'parentNodeId',
                          label: 'name',
                          order: 'seq'
                      }
                  }
              },
              calendar: {
                  defaultProperties: {
                      navigator: true
                  }
              },
              gridPanel: {
                  defaultProperties: {
                      //autoWidth: false,
                      clickToEdit: false,
                      useRightActionMenu: false,
                      editable: true,
                      isGuide: true,
                      rendererConfig: true,
                      //showExcelDialog: false,
                      excelType: 'json'
                  }
              },
              grid: {
                  defaultProperties: {
                  },
                  excelDownLoadUrl: contextPath + ruiRootPath + '/plugins/ui/grid/excelDownload.jsp'
              },
              gridScroller: {
                  defaultProperties: {
                  }
              },
              treeGrid: {
                  defaultProperties: {
                      defaultOpenTopIndex: 0,
                      fields: {
                          depthId: 'depth'
                      }
                  }
              },
              pager: {
                  defaultProperties: {
                      /* DevOn 4.0 */
                      pageSizeFieldName: 'devonRowSize',
                      viewPageStartRowIndexFieldName: 'devonTargetRow',
                      /* DevOn 3.0
                      pageSizeFieldName: 'NUMBER_OF_ROWS_OF_PAGE',
                      viewPageStartRowIndexFieldName: 'targetRow',
                      */
                      sortQueryFieldName: 'devonOrderBy'
                  }
              },
              headerContextMenu: {
              },
              pivot: {
                  defaultProperties: {
                      pivotLabelWidth: 70,
                      defaultOpenDepth: 1,
                      defaultSumColPosLast: true,
                      hiddenZeroValColumn: false
                  }
              },
              notificationManager: {
                  defaultProperties: {
                  }
              },
              api: {
                  showDetail: true
              }
          }
      ,
      project:
          {
      }
  };

  // 紐⑤뱺 �먮윭 report��.
  Rui.util.LEvent.throwErrors = true;

  Rui.BLANK_IMAGE_URL = '/resources/images/default/s.gif';
  
  Rui.onReady(function() {
      /*
       * config 愿��� �대깽�� �묒옱
       */
       var config = Rui.getConfig();
       var provider = config.getProvider();
       provider.on('stateChanged', function(e) {
           if (e.key == '$.core.defaultLocale') {
               Rui.getMessageManager().setLocale(e.value[0]);
           }
       });
       
       Rui.noticeDebug();
  });

  if(Rui.ui && Rui.ui.grid && Rui.ui.grid.LColumnModel) {
      Rui.ui.grid.LColumnModel.rendererMapper['date'] = Rui.util.LRenderer.dateRenderer('%x');
      Rui.ui.grid.LColumnModel.rendererMapper['time'] = Rui.util.LRenderer.timeRenderer();
      Rui.ui.grid.LColumnModel.rendererMapper['money'] = Rui.util.LRenderer.moneyRenderer();
      Rui.ui.grid.LColumnModel.rendererMapper['number'] = Rui.util.LRenderer.numberRenderer();
      Rui.ui.grid.LColumnModel.rendererMapper['rate'] = Rui.util.LRenderer.rateRenderer();
      Rui.ui.grid.LColumnModel.rendererMapper['popup'] = Rui.util.LRenderer.popupRenderer();
  }

  //�꾨옒�� lMultiCombo�� '�좏깮�섏꽭��'硫붿떆吏� �ㅺ뎅�� 泥섎━�대굹, �ㅺ뎅�댁쿂由щ� �꾪븳 MessageManager 濡쒕뵫 �쒓컙�뚮Ц�� �ъ슜�좎닔 �녿떎.
  //�꾨옒瑜� �꾨줈�앺듃 怨듯넻�쇰줈 ��만 寃쎌슦 LCombo�� emptyMessage�� 以��섎뒗 LMultiCombo�� placeholder瑜� �ъ슜�� �� �덈떎.
  //Rui.getConfig().set('$.ext.multicombo.defaultProperties.placeholder', Rui.getMessageManager().get('$.base.msg108'));
  
})();
