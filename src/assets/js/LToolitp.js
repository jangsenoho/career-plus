/*
 * @(#) LTooltip.js
 *
 * DevOn RUI
 * Build Version : 2.7
 * 
 * Copyright �� LG CNS, Inc. All rights reserved.
 *
 * devon@lgcns.com
 * http://www.dev-on.com
 *
 * Do Not Erase This Comment!!! (�� 二쇱꽍臾몄쓣 吏��곗� 留먭쾬)
 *
 * rui/license.txt瑜� 諛섎뱶�� �쎌뼱蹂닿퀬 �ъ슜�섏떆湲� 諛붾엻�덈떎. License.txt�뚯씪�� �덈�濡� ��젣�섏떆硫� �덈맗�덈떎. 
 *
 * 1. �щ궡 �ъ슜�� KAMS瑜� �듯빐 �붿껌�섏뿬 �ъ슜�덇�瑜� 諛쏆쑝�붿빞 �뚰봽�몄썾�� �쇱씠�쇱뒪 怨꾩빟�쒖뿉 �숈쓽�섎뒗 寃껋쑝濡� 媛꾩＜�⑸땲��.
 * 2. DevOn RUI媛� �ы븿�� �쒗뭹�� �먮ℓ�섏떎 寃쎌슦�먮룄 KAMS瑜� �듯빐 �붿껌�섏뿬 �ъ슜�덇�瑜� 諛쏆쑝�붿빞 �⑸땲��.
 * 3. KAMS瑜� �듯빐 �ъ슜�덇�瑜� 諛쏆� �딆� 寃쎌슦 �뚰봽�몄썾�� �쇱씠�쇱뒪 怨꾩빟�� �꾨컲�� 寃껋쑝濡� 媛꾩＜�⑸땲��.
 * 4. 蹂꾨룄濡� �먮ℓ�� 寃쎌슦�� LGCNS�� �뚰봽�몄썾�� �먮ℓ�뺤콉�� �곕쫭�덈떎. (KAMS�� 臾몄쓽 諛붾엻�덈떎.)
 *
 * (二쇱쓽!) �먯��먯쓽 �덈씫�놁씠 �щ같�� �� �� �놁쑝硫�
 * LG CNS �몃�濡� �좎텧�댁꽌�� �덈맗�덈떎.
 * 
 * (異붽�) Plugin �섏쐞 紐⑸줉�� �ы븿�� �뚯씪�� 寃쎌슦 �꾨줈�앺듃�먯꽌 �꾩쓽濡� customizing �댁꽌 �ъ슜 媛��ν빀�덈떎.
 * ��, �꾨줈�앺듃�먯꽌 customizing �� �ы빆�� 湲곗닠吏��� ���� 踰붿쐞�먯꽌 �쒖쇅�⑸땲��.
 * 
 */

(function(){
  var Event = Rui.util.LEvent;
  Rui.namespace('Rui.ui');
  Rui.ui.LTooltip = function(config){
      Rui.ui.LTooltip.superclass.constructor.call(this, config);
  };
  Rui.extend(Rui.ui.LTooltip, Rui.ui.LUIComponent, {
      otype: 'Rui.ui.LTooltip',
      CSS_BASE: 'L-stt',
      id: null,
      showdelay: 500,
      autodismissdelay: 5000,
      showmove: false,
      context: null,
      text: null,
      gridPanel: null,
      margin: 5,
      initComponent: function(config){
          Rui.ui.LTooltip.superclass.initComponent.call(this);
          if(!this.id) this.id = (config && config.id) || Rui.id();
      },
      initDefaultConfig: function(){
          Rui.ui.LTooltip.superclass.initDefaultConfig.call(this);
          this.cfg.addProperty('showdelay', {
              value: this.showdelay,
              validator: Rui.isNumber
          });
          this.cfg.addProperty('autodismissdelay', {
              value: this.autodismissdelay,
              validator: Rui.isNumber
          });
      },
      initEvents: function(){
          this.ctxEl = Rui.get(this.context);
          this._createEvents();
      },
      gridEvents: function (grid){
          this.gridPanel = grid;
          this.ctxEl = this.gridPanel.getView().el;
          this._createEvents();
      },
      _createEvents: function(){
          if(this.ctxEl == null) return;
          this.ctxEl.on('mouseover', this.onContextMouseOver, this, true, {system: true});
          if(this.showmove){
              this.ctxEl.on('mousemove', this.onContextMouseMove, this, true, {system: true});
          }
          this.ctxEl.on('mouseout', this.onContextMouseOut, this, true, {system: true});
      },
      _createElement: function(){
          var ttEl = document.createElement('div');
          ttEl.id = this.id;
          ttEl = Rui.get(ttEl);
          ttEl.addClass(this.CSS_BASE);
          ttEl.addClass('L-fixed');
          ttEl.addClass('L-hide-display');
          if(Rui.useAccessibility()){
              ttEl.setAttribute('role', 'tooltip');
              ttEl.setAttribute('aria-hidden', true);
          }
          document.body.appendChild(ttEl.dom);
          this.ttEl = ttEl;
          if(this.showmove){
            this.ttEl.on('mousemove', this.onTooltipMouseMove, this, true, {system: true});
          }
          this.ttEl.on('mouseout', this.onTooltipMouseOut, this, true, {system: true});
      },
      onContextMouseOver: function(e){
          if(this.oldDom === e.target) return; 
          this.pageX = Event.getPageX(e);
          this.pageY = Event.getPageY(e);
          var cell = e.target;
          if(!this.ttEl) this._createElement();
          if(!this.gridPanel){
              var contextEl = this.ctxEl;
              if(contextEl.dom.title){
                  this._tempTitle = contextEl.dom.title;
                  contextEl.dom.title = '';
              }
          } else {
              var view = this.gridPanel.getView();
              var idx = view.findCell(cell, Event.getPageX(e));
              if(idx < 0) return;
              var cm = view.getColumnModel();
              var col = cm.getColumnAt(idx, true);
              var row = view.findRow(cell, Event.getPageY(e), false);
              var val = cm.getCellConfig(row, idx, 'tooltipText');
              if(col !== undefined && row > -1){
                  var c = Rui.get(cell);
                  if( c.hasClass('L-grid-cell-tooltip') || Rui.get(c.dom.parentNode).hasClass('L-grid-cell-tooltip')){
                      this.text = (val === undefined) ? col.tooltipText : val;
                      if(Rui.isEmpty(this.text)){
                          if(cell.rowSpan > 1){
                              for (var i = 1; i < cell.rowSpan; i++){
                                  val = cm.getCellConfig(row + i, idx, 'tooltipText');
                                  if(val) break;
                              }
                          }
                      }
                  } else this.text = '';
              } else {
                  this.text = '';
              }
              this.text = this.text ? Rui.trim(this.text.toString()) : this.text;
              if(idx == -1 || Rui.isEmpty(this.text)) return;
          }
          if(this.delayShow){
              this.delayShow.cancel();
              delete this.delayShow;
          }
          this.delayShow = Rui.later(this.showdelay, this, this.show, this);
          if(this.autodismissShow){
              this.autodismissShow.cancel();
              delete this.autodismissShow;
          }
          this.autodismissShow = Rui.later(this.autodismissdelay, this, this.hide, this);
          this.oldDom = cell;
      },
      onContextMouseMove: function(e){
          if(this.delayShow == undefined) return;
          this.pageX = Event.getPageX(e);
          this.pageY = Event.getPageY(e);
          if(this.showmove){
              this.setTooltipXY();
          }
      },
      onTooltipMouseMove: function(e){
          this.onContextMouseMove(e);
          if(!this.isPointerStillShowing())
              this.hide();
      },
      onContextMouseOut: function(e){
          this.pageX = Event.getPageX(e);
          this.pageY = Event.getPageY(e);
          if(Rui.get(e.target).hasClass('L-grid-body')) return;
          if(this._tempTitle){
              this.ctxEl.dom.title = this._tempTitle;
              this._tempTitle = null;
          }
          if(!this.isPointerStillShowing()){
            this.hide();
          }else{
              this.hide();
              if(this.delayShow){
                  this.delayShow.cancel();
                  delete this.delayShow;
              }
          }
          this.oldDom = null;
      },
      onTooltipMouseOut: function(e){
          this.onContextMouseOut(e);
      },
      isPointerStillShowing: function(){
          var pt = new Rui.util.LPoint(this.pageX, this.pageY),
              region = this.ctxEl.getRegion();
          if(region && region.contains(pt)) return true;
          return false;
      },
      setTooltipXY: function(){
          var h = this.ttEl.getHeight() || 0,
              w = this.ttEl.getWidth(),
              t = this.pageY,
              l = this.pageX,
              vp = Rui.util.LDom.getViewport(),
              vw = vp.width + document.body.scrollLeft;
              vh = vp.height + document.body.scrollTop;
          if(w < 1 || h < 1){
            t = this.ctxEl.getTop();
            l = this.ctxEl.getLeft();
          }else{
            if(vh <= (t + h + this.margin)) t = vh - h;
            else t += this.margin;
            if(vw <= (l + w + this.margin)) l = vw - w;
            else l += this.margin;
          }
          this.ttEl.setTop(t);
          this.ttEl.setLeft(l);
      },
      show: function(){
          Rui.get(this.id).html(this.text);
          this.ttEl.removeClass('L-hide-display');
          this.width = this.ttEl.getWidth();
          if(Rui.useAccessibility()) this.ttEl.setAttribute('aria-hidden', false);
          if(this.pageX > 0 && this.pageY > 0){
              this.setTooltipXY();
          }
      },
      hide: function(){
          if(Rui.isUndefined(this.ttEl)) return false;
          this.ttEl.addClass('L-hide-display');
          if(Rui.useAccessibility()) this.ttEl.setAttribute('aria-hidden', true);
          if(this.delayShow){
              this.delayShow.cancel();
              delete this.delayShow;
          }
          if(this.autodismissShow){
              this.autodismissShow.cancel();
              delete this.autodismissShow;
          }
      },
      setText: function(text){
          this.text = text;
      },
      destroy: function(){
          if(this.ttEl){
              this.ttEl.unOnAll();
              this.ttEl.remove();
              this.ttEl = null;
          }
          this.ctxEl.unOnAll();
          Rui.ui.LTooltip.superclass.destroy.call(this);
      }
  });
  })();